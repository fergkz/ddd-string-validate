<?php

namespace Application\Controller;

use stdClass;
use Exception;
use Domain\Command\ValidateCommand;
use Domain\Entity\BlackListEntityCollectionFinal;
use Domain\Entity\BlackListEntityFinal;

class StringValidateController
{
    private $bodyContent;

    public function verifyString ()
    {
        try {
            $this->loadBodyContent();
            $this->validate();
            
            $wordsCollection = new BlackListEntityCollectionFinal();
            foreach ($this->bodyContent->words as $word) {
                $wordsCollection->push(new BlackListEntityFinal($word));
            }
            
            $validCollection = ValidateCommand::validateCollection($wordsCollection);
            
            $response = (Object) [
                "words" => []
            ];

            foreach ($validCollection->getCollection() as $valid) {
                $response->words[$valid->getTextString()] = [
                    "word" => $valid->getTextString(),
                    "valid" => $valid->isValid(),
                    "message" => $valid->getMessage()
                ];
            }

            self::jsonResponse($response);
        } catch (Exception $e) {
            self::jsonResponse((Object) [
                "error" => $e->getMessage()
            ]);
        }
    }

    private function validate ()
    {
        return true;
    }

    private static function jsonResponse (stdClass $output)
    {
        header('Content-Type: application/json');
        echo json_encode($output);
        exit;
    }

    private function loadBodyContent ()
    {
        $this->bodyContent = json_decode(file_get_contents('php://input'));
        if (!$this->bodyContent) {
            throw new Exception("The body content is not valid");
        }
    }
}

