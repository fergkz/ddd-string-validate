<?php

namespace Domain\Repository;

use Domain\Entity\BlackListEntityCollectionFinal;
use Domain\Entity\BlackListEntityFinal;

interface BlackListRepositoryAdapterInterface
{
    public function listAll () : BlackListEntityCollectionFinal;

    public function getByTextString (String $textString) : BlackListEntityFinal;
}