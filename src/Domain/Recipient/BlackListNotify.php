<?php

namespace Domain\Recipient;

use stdClass;

final class BlackListNotify
{
    private $message;
    private $author;

    public function __construct(String $message, stdClass $author)
    {
        $this->message = $message;
        $this->author = (Object) [
            "name" => $author->name ?? "",
            "email" => $author->email ?? ""
        ];
    }

    public function getMessage () : String
    {
        return $this->message;
    }

    public function getAuthor () : stdClass
    {
        return $this->author;
    }
}