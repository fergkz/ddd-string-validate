<?php

namespace Domain\Repository;

use Domain\Recipient\BlackListNotify;

interface BlackListNotifyAdapterInterface
{
    public function handle (BlackListNotify $blackListNotify) : bool;
}