<?php

namespace Domain\Command;

use Domain\Entity\BlackListEntity;
use Domain\Entity\BlackListEntityCollectionFinal;
use Domain\Entity\BlackListEntityFinal;
use Domain\Recipient\BlackListNotify;
use Domain\Repository\BlackListRepositoryAdapterInterface;
use Domain\Repository\BlackListRepositoryPort;
use Infrastructure\CommandBus;
use stdClass;

class ValidateCommand
{
    private $blackListEntity;

    public function __construct(BlackListEntity $blackListEntity)
    {
        $this->blackListEntity = $blackListEntity;
    }

    public function validate () : BlackListEntityFinal
    {
        $repository = CommandBus::retrieve(BlackListRepositoryAdapterInterface::class);
        $entity = $repository->getByTextString($this->blackListEntity->getTextString());
        
        if ($entity && $entity->getTextString()) {
            $this->blackListEntity->setValidate(false, $entity->getMessage());
        } else {
            $this->blackListEntity->setValidate(true, "String not found in blacklist");
        }
        
        $notify = new BlackListNotify(
            "test message",
            (Object) [
                "name" => "System",
                "email" => "noreplay@test.madeiramadeira.com.br"
            ]
        );
        CommandBus::dispatch($notify);

        return $this->blackListEntity;
    }

    public static function validateCollection (BlackListEntityCollectionFinal $collection) : BlackListEntityCollectionFinal
    {
        foreach ($collection->getCollection() as $blackListEntity) {
            $command = new ValidateCommand($blackListEntity);
            $command->validate();
        }
        return $collection;
    }
}