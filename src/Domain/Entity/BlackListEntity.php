<?php

namespace Domain\Entity;

class BlackListEntity
{
    protected $textString = "";
    protected $valid = false;
    protected $message = "";

    public function __construct(String $text)
    {
        $this->setTextString($text);
    }

    public function setTextString (String $text)
    {
        $this->textString = $text;
    }

    public function getTextString () : String
    {
        return $this->textString;
    }

    public function isValid () : Bool
    {
        return $this->valid;
    }

    public function getMessage () : String
    {
        return $this->message;
    }

    final public function setValidate (Bool $valid, String $message = "")
    {
        $this->valid = $valid;
        $this->message = $message;
    }
}