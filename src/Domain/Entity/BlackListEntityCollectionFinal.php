<?php

namespace Domain\Entity;

final class BlackListEntityCollectionFinal
{
    private $collection = [];

    public function push (BlackListEntityFinal $blackListEntity)
    {
        $this->collection[] = $blackListEntity;
    }

    public function getCollection ()
    {
        return $this->collection;
    }
}