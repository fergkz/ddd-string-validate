<?php

namespace Infrastructure\Recipient\BlackListNotify;

use Domain\Recipient\BlackListNotify;
use Domain\Repository\BlackListNotifyAdapterInterface;

class NotifyEmail implements BlackListNotifyAdapterInterface
{
    public function handle (BlackListNotify $blackListNotify) : bool
    {
        self::log("in handle of NotifyEmail");
        self::log($blackListNotify);
        return false;
    }

    private static function log ($data)
    {
        $filename = realpath(__DIR__ . "/../../../../bucket") . DIRECTORY_SEPARATOR . "log.txt";
        $content = file_exists($filename) ? file_get_contents($filename) : "";
        file_put_contents($filename, $content . "\n" . print_r($data, true));
    }
}
