<?php

namespace Infrastructure;

use Exception;

class CommandBus
{
    private static $configDispatch = [
        'Domain\\Recipient\\BlackListNotify' => [
            'Infrastructure\\Recipient\\BlackListNotify\\NotifySNS',
            'Infrastructure\\Recipient\\BlackListNotify\\NotifyEmail'
        ],
        'Domain\\Recipient\\ErrorNotify' => [
            'Infrastructure\\Recipient\\ErrorNotify\\NotifySNS',
            'Infrastructure\\Recipient\\ErrorNotify\\NotifyEmail'
        ]
    ];
    
    private static $configRetrieve = [
        'Domain\\Repository\\BlackListRepositoryAdapterInterface' => 'Infrastructure\\Repository\\BlackList\\MockRepository'
    ];

    public static function dispatch ($object)
    {
        $class = get_class($object);
        if (isset(self::$configDispatch[$class])) {
            foreach (self::$configDispatch[$class] as $adapterClass)
            {
                if (!class_exists($adapterClass)) {
                    throw new Exception("Config to $adapterClass not found");
                }
                $call = new $adapterClass();
                $call->handle($object);
            }
        } else {
            throw new Exception("Config to $class not found");
        }
    }

    public static function retrieve ($class)
    {
        if (isset(self:: $configRetrieve[$class])) {
            $adapterClass = self:: $configRetrieve[$class];
            if (!class_exists($adapterClass)) {
                throw new Exception("Config to $adapterClass not found");
            }
            return new $adapterClass();
        } else {
            throw new Exception("Config to $class not found");
        }
    }
}
