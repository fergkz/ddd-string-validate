<?php

namespace Infrastructure\BlackList;

use Domain\Entity\BlackListEntityCollectionFinal;
use Domain\Entity\BlackListEntityFinal;
use Domain\Repository\BlackListRepositoryAdapterInterface;

class PersistenceRepository implements BlackListRepositoryAdapterInterface
{
    public function listAll(): BlackListEntityCollectionFinal
    {
        return new BlackListEntityCollectionFinal;
    }

    public function getByTextString(String $textString): BlackListEntityFinal
    {
        return new BlackListEntityFinal("");
    }
}
