<?php

namespace Infrastructure\Repository\BlackList;

use Domain\Entity\BlackListEntityCollectionFinal;
use Domain\Entity\BlackListEntityFinal;
use Domain\Repository\BlackListRepositoryAdapterInterface;

class MockRepository implements BlackListRepositoryAdapterInterface
{
    private $filename = null;
    private $filedata = null;

    public function __construct()
    {
        $this->filename = realpath(__DIR__ . "/../../../../bucket") . DIRECTORY_SEPARATOR . "repository-blacklist-mock.json";
        if (!file_exists($this->filename)) {
            file_put_contents($this->filename, "[]");
        }
        $this->filedata = json_decode(file_get_contents($this->filename));
    }

    public function listAll () : BlackListEntityCollectionFinal
    {
        $collection = new BlackListEntityCollectionFinal;
        // foreach (self::$fakeBlackList as $word => $reason) {
        //     $entity = new BlackListEntityFinal($word);
        //     $entity->setValidate(false, $reason);
        //     $collection->push($entity);
        // }

        return $collection;
    }

    public function getByTextString (String $textString) : BlackListEntityFinal
    {
        $entity = new BlackListEntityFinal("");

        $data = array_filter($this->filedata, function ($row) use ($textString) { return $row->word === $textString; });
        $dataKey = key($data);
        $data = !is_null($dataKey) ? $data[$dataKey] : null;

        if ($data) {
            $entity = new BlackListEntityFinal($data->word);
            $entity->setValidate($data->valid, $data->reason ?? "");
        }

        return $entity;
    }
}