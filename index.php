<?php

ini_set("display_errors", E_ALL);

include "./vendor/autoload.php";

$routes = [
    "/validate/verify" => ["Application/Controller/StringValidateController", "verifyString"]
];

if (isset($_SERVER["REQUEST_URI"])) {
    $pathinfo = $_SERVER["PATH_INFO"] ?? "";
    $cleanRoute = "/".trim($pathinfo, "\\..\/");
    if (isset($routes[$cleanRoute])) {
        $class = str_replace("/", "\\", $routes[$cleanRoute][0]);
        $controller = new $class;
        $controller->{$routes[$cleanRoute][1]}();
    }
}

die("Failed on load URI");